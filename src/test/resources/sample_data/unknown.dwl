[
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "Anchorage",
    "toCityCodes": "ANCA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-17",
    "price": 380,
    "Carrier": "Alaska Airlines"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "Barrow",
    "toCityCodes": "BRWA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-24",
    "price": "N/A",
    "Carrier": "No Flights found for the given dates"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "Aspen",
    "toCityCodes": "ASEA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-24",
    "price": "N/A",
    "Carrier": "No Flights found for the given dates"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "Colorado Springs",
    "toCityCodes": "COSA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-17",
    "price": 243,
    "Carrier": "Frontier Airlines"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "Denver",
    "toCityCodes": "DENA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-17",
    "price": 128,
    "Carrier": "Frontier Airlines"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "Durango",
    "toCityCodes": "DROA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-24",
    "price": "N/A",
    "Carrier": "No Flights found for the given dates"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "Eagle",
    "toCityCodes": "EGEA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-24",
    "price": "N/A",
    "Carrier": "No Flights found for the given dates"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "Grand Junction",
    "toCityCodes": "GJTA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-24",
    "price": "N/A",
    "Carrier": "No Flights found for the given dates"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "Kansas City",
    "toCityCodes": "MKCA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-17",
    "price": 204,
    "Carrier": "Southwest Airlines"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "Kansas City",
    "toCityCodes": "MKCA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-17",
    "price": 274,
    "Carrier": "United"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "Springfield",
    "toCityCodes": "SGFA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-17",
    "price": 367,
    "Carrier": "United"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "Albany",
    "toCityCodes": "ALBA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-24",
    "price": "N/A",
    "Carrier": "No Flights found for the given dates"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "Oakland",
    "toCityCodes": "OAKA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-17",
    "price": 224,
    "Carrier": "jetBlue"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "Ontario",
    "toCityCodes": "ONTA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-17",
    "price": 293,
    "Carrier": "Frontier Airlines"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "Ontario",
    "toCityCodes": "ONTA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-17",
    "price": 2718,
    "Carrier": "Linear Air"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "Palm Springs",
    "toCityCodes": "PSPA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-17",
    "price": 223,
    "Carrier": "Alaska Airlines"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "Redding",
    "toCityCodes": "RDDA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-24",
    "price": "N/A",
    "Carrier": "No Flights found for the given dates"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "Sacramento",
    "toCityCodes": "SACA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-17",
    "price": 249,
    "Carrier": "jetBlue"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "Sacramento",
    "toCityCodes": "SACA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-17",
    "price": 1168,
    "Carrier": "Linear Air"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "San Diego",
    "toCityCodes": "SANA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-17",
    "price": 182,
    "Carrier": "Alaska Airlines"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "San Francisco",
    "toCityCodes": "SFOA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-24",
    "price": "N/A",
    "Carrier": "No Flights found for the given dates"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "San Jose",
    "toCityCodes": "SJCA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-17",
    "price": 229,
    "Carrier": "jetBlue"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "San Luis Obispo",
    "toCityCodes": "CSLA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-17",
    "price": 284,
    "Carrier": "United"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "Santa Ana",
    "toCityCodes": "SNAA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-17",
    "price": 152,
    "Carrier": "Alaska Airlines"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "Santa Barbara",
    "toCityCodes": "SBAA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-17",
    "price": 254,
    "Carrier": "United"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "Santa Maria",
    "toCityCodes": "SMXA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-17",
    "price": 385,
    "Carrier": "Alaska Airlines"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "Santa Maria",
    "toCityCodes": "SMXA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-17",
    "price": 2141,
    "Carrier": "Linear Air"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "Daytona Beach",
    "toCityCodes": "DABA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-17",
    "price": 451,
    "Carrier": "Delta"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "Fort Lauderdale",
    "toCityCodes": "FLLA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-17",
    "price": 185,
    "Carrier": "Frontier Airlines"
  },
  {
    "fromCity": "SAN",
    "fromCityCode": "SFOA-sky",
    "toCity": "Fort Lauderdale",
    "toCityCodes": "FLLA-sky",
    "date_from": "2019-05-17",
    "date_to": "2019-05-17",
    "price": 280,
    "Carrier": "jetBlue"
  }
]